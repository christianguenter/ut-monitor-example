# UT monitor example for abap mockup loader repository hosted at gitlab

## Preamble and problem definition

- Supposing you have:
  - a `test` dir with excels and includes
  - it compiles into `zmockup_loader_example` SMW0 object within your package
- Both SMW0 object (zip file) and Excels are binary files and you don't see changes between commits
- But you want to control such changes
- This example provide a CI script to enable it

## Solution in words

- you will have 2 additional branches - `smw0-monitor` and `ut-monitor`
- on every commit gitlab CI will check
  - if your SMW0 object changed, it will unpack it's content and auto-commit to `smw0-monitor` branch
  - if files in your test dir (excels or includes) changed, it will run [mockup-compiler-js](https://github.com/sbcgua/mockup-compiler-js) to build a txt file representation of your tests and commit it to `ut-monitor` branch
- thus all you changes are rendered as txt files and diffs are git-visible

In this example the following naming is used (you may used the same adjust to your desire):

- the dir to compile test data to - `uncompressed`
- additional branches names: `smw0-monitor` (for SMW0 object tracking) and `ut-monitor` (for Excel sources tracking)
- the dir with test files (excel) - `test` in the root dir
- the SMW0 object name `zmockup_loader_example`, and consequently the abapGit file name `zmockup_loader_example.w3mi.data.zip`

## Cooking

Preparations

- create an SSH deployment key and install it to your gitlab repo, in particular, create `SSH_PRIVATE_KEY` env var with SSH secret (preferable do not use this key anywhere else, it should be just for one repo)
  - in a linux shell: `ssh-keygen -t ed25519 -C "ut-monitor-example"` where `ut-monitor-example` is your repo name or any other label for the key
  - copy `.ssh/id_ed25519.pub` content to gitlab "Settings -> Repository -> Deploy keys" with write access allowed
  - copy `.ssh/id_ed25519` content to "Settings -> CD/CI -> Variables" - SSH_PRIVATE_KEY, it must be protected
- To compile txt files from excel, you need `.mock-config.json` setup for your repo - see example in this repo and/or docs at [mockup-compiler-js](https://github.com/sbcgua/mockup-compiler-js)
- In the latest version of the script, the target branches are created automatically so just skip to `CI script` section below. Otherwise you can also create branches and their initial state as described below if you'd like to have additional control
- preferably protect the new branches in your gitlab repo settings so that rebase is not possible (after they are created)

Manually creating and populating branches with current state [optional]

- add `uncompressed/` line to your `.gitignore`
- `.gitattributes` file to specify line ending for you files, especially for txt, which will be the output of test compilation (`lf` is recommended)
- initialize 2 orphan branches - `smw0-monitor` and `ut-monitor`
  - `git checkout --orphan ut-monitor`
  - `git rm -rf .`
  - `-- create .gitignore and .gitattributes - see below --`
  - `git add .gitignore .gitattributes`
  - `git commit -a -m "Initial Commit"`
  - `git push origin ut-monitor`
  - `-- repeat for smw0-monitor branch --`
- create branches - each branch must have:
  - `.gitignore`, ignoring everything but git related configs and `uncompressed` dir
  - `.gitattributes` file to specify line ending for you files
- you may potentially skip this section but then you will see first changes only after **second** change, because the first change that happen just copy all the content to the branch as new. To exec the below you should have:
  - a linux-like environment (windows git-bash should do by the way)
  - scripts assume you start on master branch
  - assume namings used in this examples
  - assume you have access to `..` dir
  - assuming you have `nodejs` installed for the ut script
- to pre-populate SMW0 branch:
  - `unzip ./src/zmockup_loader_example.w3mi.data.zip -d ../uncompressed`
  - `git checkout smw0-monitor`
  - `mv ../uncompressed .`
  - `git add *`
  - `git commit -m "Initial state"`
  - `git push --set-upstream origin smw0-monitor`
- to pre-populate UT monitor branch
  - `npm -g i mockup-compiler-js`
  - `mockup-compiler`
  - `mv uncompressed ..`
  - `git checkout ut-monitor`
  - `mv ../uncompressed .`
  - `git add *`
  - `git commit -m "Initial state"`
  - `git push --set-upstream origin ut-monitor`
- OR you can also skip the `changes` param in the script below initially, then the branches will be rebuild on the first activation of CI (much simpler approach)

CI script (`.gitlab-ci.yml`)

- The simplest option is to copy the script from the repo and adjust variables
  - _UNCOMPRESSED_PATH - dir name for compiled/uncompressed unit tests in monitor branches
  - _TARGET_BRANCH - branch name for smw0 monitor
  - _SMW0_OBJ_PATH - path to SMW0 zip object
  - _COMMIT_USER_EMAIL - an email to commit changes to monitor branches with (can be arbitrary e.g. bot@your.domain)
  - _COMMIT_USER_NAME - an user to commit changes to monitor branches with (can be arbitrary e.g. bot)
- the remaining part are `smw0-monitor` and `ut-monitor` jobs that rely on standard template from [ut-monitor-routines](https://gitlab.com/atsybulsky/mockup-loader-gitlab-ci/raw/master/ut-monitor-routines.yml)
- you may want to have more control over your script - e.g. change how commit message looks - then check the [ut-monitor-routines](https://gitlab.com/atsybulsky/mockup-loader-gitlab-ci/raw/master/ut-monitor-routines.yml) and create your own monitor jobs based on `ut-monitor-default` and `smw0-monitor-default` (e.g. replace the `*create_commit_message` into your preferred way)
